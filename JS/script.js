/*Подія input запускається щоразу після того, як користувач змінює значення.
На відміну від подій клавіатури, input запускається при будь-якій зміні значень, навіть тих, які не передбачають дії клавіатури: 
вставлення тексту за допомогою миші або використання розпізнавання мовлення для диктування тексту.*/

const btn = document.querySelectorAll('.btn');
btn.forEach(function (element) {
    element.setAttribute('data-name', element.innerText);
});

document.addEventListener('keydown', function (event) {
    console.log(event.key)
    btn.forEach(function (element) {
        element.style.backgroundColor = 'black';
        if (element.dataset.name === event.key.toUpperCase() || element.dataset.name === event.key) {
            element.style.backgroundColor = 'blue';
        };
    });

});